#!/bin/bash

pkill -f "Djetty.port=8983"
./status.sh
while [ `./status.sh | grep -i 'running' | wc -l` -gt 0 ]; do
    sleep 1;
    ./status.sh
done;
