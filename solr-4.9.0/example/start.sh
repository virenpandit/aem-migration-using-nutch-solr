#!/bin/bash

if [ `pgrep -f "Djetty.port=8983" | wc -l` -gt 0 ]; then
    echo "ERROR! Solr Already Running!";
else
    rm -rf ./logs/*.log*
    nohup java -Djetty.port=8983 -DzkRun -jar start.jar 1>logs/server.log 2>&1 &
fi;
