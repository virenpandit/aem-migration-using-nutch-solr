#!/bin/bash

if [ `pgrep -f "Djetty.port=8983" | wc -l` -gt 0 ]; then
    echo "ERROR! Solr Already Running!"
else
    rm -rf ./logs/*.log
    rm -rf ./solr/zoo_data
    rm -rf ./solr/collection1/data
    rm -rf ./solr/tryp/data
    nohup java -Djetty.port=8983 -Dbootstrap_confdir=./solr/tryp/conf -Dcollection.configName=tryp -DzkRun -jar start.jar 1>logs/server.log 2>&1 &
fi;
