#!/bin/bash

if [ `pgrep -f "Djetty.port=8983" | wc -l` -gt 0 ]; then
    echo "Solr is running with the PID: `pgrep -f 'Djetty.port=8983'`"
else
    echo "Solr is Down"
fi;

