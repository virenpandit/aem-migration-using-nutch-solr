# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements. See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define some default values that can be overridden by system properties
hadoop.log.dir=.
hadoop.log.file=hadoop.log

# RootLogger - DailyRollingFileAppender
log4j.rootLogger=INFO,DRFA

# Logging Threshold
log4j.threshold=ALL

#special logging requirements for some commandline tools
log4j.logger.org.apache.nutch.crawl.Crawl=INFO
log4j.logger.org.apache.nutch.crawl.Injector=INFO
log4j.logger.org.apache.nutch.crawl.Generator=INFO
log4j.logger.org.apache.nutch.fetcher.Fetcher=DEBUG
log4j.logger.org.apache.nutch.net=DEBUG
log4j.logger.org.apache.nutch.parse.ParseSegment=INFO
log4j.logger.org.apache.nutch.crawl.CrawlDbReader=INFO,cmdstdout
log4j.logger.org.apache.nutch.crawl.CrawlDbMerger=INFO
log4j.logger.org.apache.nutch.crawl.LinkDbReader=INFO
log4j.logger.org.apache.nutch.segment.SegmentChecker=INFO
log4j.logger.org.apache.nutch.segment.SegmentReader=INFO
log4j.logger.org.apache.nutch.segment.SegmentMerger=INFO
log4j.logger.org.apache.nutch.crawl.CrawlDb=INFO
log4j.logger.org.apache.nutch.crawl.LinkDb=INFO
log4j.logger.org.apache.nutch.crawl.LinkDbMerger=INFO
log4j.logger.org.apache.nutch.indexer.IndexingJob=INFO
log4j.logger.org.apache.nutch.indexer.solr.SolrIndexer=INFO
log4j.logger.org.apache.nutch.indexer.solr.SolrWriter=INFO
log4j.logger.org.apache.nutch.indexer.solr.SolrDeleteDuplicates=INFO
log4j.logger.org.apache.nutch.indexer.solr.SolrClean=INFO
log4j.logger.org.apache.nutch.scoring.webgraph.WebGraph=INFO
log4j.logger.org.apache.nutch.scoring.webgraph.LinkRank=INFO
log4j.logger.org.apache.nutch.scoring.webgraph.Loops=INFO
log4j.logger.org.apache.nutch.scoring.webgraph.ScoreUpdater=INFO
log4j.logger.org.apache.nutch.util.hostdb.HostDb=INFO
log4j.logger.org.apache.nutch.util.hostdb.DumpHostDb=INFO
log4j.logger.org.apache.nutch.parse.ParserChecker=INFO
log4j.logger.org.apache.nutch.indexer.IndexingFiltersChecker=INFO
log4j.logger.org.apache.nutch.tools.FreeGenerator=INFO
log4j.logger.org.apache.nutch.util.domain.DomainStatistics=INFO
log4j.logger.org.apache.nutch.tools.CrawlDBScanner=INFO
log4j.logger.org.apache.nutch.plugin.PluginRepository=WARN
log4j.logger.org.apache.nutch.indexer.wyngrep=DEBUG
log4j.logger.org.apache.nutch.parser.wyngrep=DEBUG
log4j.logger.com.atlantbh.nutch.filter=DEBUG
log4j.logger.com.atlantbh.nutch.filter.xpath=DEBUG
log4j.logger.com.atlantbh.nutch.filter.index.omit=DEBUG
log4j.logger.org.apache.nutch.indexwriter.aem=DEBUG

log4j.logger.org.apache.nutch=INFO
log4j.logger.org.apache.hadoop=WARN

#
# Daily Rolling File Appender
#

log4j.appender.DRFA=org.apache.log4j.DailyRollingFileAppender
log4j.appender.DRFA.File=${hadoop.log.dir}/${hadoop.log.file}

# Rollver at midnight
log4j.appender.DRFA.DatePattern=.yyyy-MM-dd

# 30-day backup
#log4j.appender.DRFA.MaxBackupIndex=30
log4j.appender.DRFA.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
log4j.appender.DRFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} - %m%n
# Debugging Pattern format: Date LogLevel LoggerName (FileName:MethodName:LineNo) LogMessage
#log4j.appender.DRFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} (%F:%M(%L)) - %m%n


#
# stdout
# Add *stdout* to rootlogger above if you want to use this 
#

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
# log4j.appender.stdout.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} (%F:%M(%L)) - %m%n
log4j.appender.stdout.layout.ConversionPattern=%d{ISO8601} %-5p (%F:%M(%L)) - %m%n

#
# plain layout used for commandline tools to output to console
#
log4j.appender.cmdstdout=org.apache.log4j.ConsoleAppender
log4j.appender.cmdstdout.layout=org.apache.log4j.PatternLayout
log4j.appender.cmdstdout.layout.ConversionPattern=%m%n

#
# Rolling File Appender
#

log4j.appender.RFA=org.apache.log4j.RollingFileAppender
log4j.appender.RFA.File=${hadoop.log.dir}/${hadoop.log.file}

# Logfile size and and 30-day backups
log4j.appender.RFA.MaxFileSize=1MB
log4j.appender.RFA.MaxBackupIndex=30

log4j.appender.RFA.layout=org.apache.log4j.PatternLayout
log4j.appender.RFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} - %m%n
log4j.appender.RFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} (%F:%M(%L)) - %m%n

