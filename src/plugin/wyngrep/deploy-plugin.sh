#!/bin/bash

/bin/cp -rf /opt/nutch/build/wyngrep /opt/nutch/runtime/local/plugins/
echo 'JAR file copied from [/opt/nutch/build/wyngrep] to [/opt/nutch/runtime/local/plugins/]'

/bin/cp -rf /opt/nutch/src/plugin/wyngrep/plugin.xml /opt/nutch/runtime/local/plugins/wyngrep/
echo 'Plugin XML copied from [/opt/nutch/src/plugin/wyngrep/plugin.xml] to [/opt/nutch/runtime/local/plugins/wyngrep/]'

NUTCH_RUNTIME=/opt/nutch/runtime/local
for i in `ls ./conf/*`; do
    RAW_FILENAME=`basename $i`
    if [ -f "$NUTCH_RUNTIME/conf/$RAW_FILENAME" ]; then
        echo "OVERWRITING configuration-xml: [conf/$RAW_FILENAME]"
    else 
        echo "Copying configuration-xml: [conf/$RAW_FILENAME]"
    fi;
    /bin/cp -rf $i $NUTCH_RUNTIME/conf/$RAW_FILENAME
done;
for i in `ls ./lib/*`; do
    RAW_FILENAME=`basename $i`
    if [ ! -f "$NUTCH_RUNTIME/plugins/wyngrep/$RAW_FILENAME" ]; then
        echo "Copying dependency: [$RAW_FILENAME]"
        /bin/cp -rf $i $NUTCH_RUNTIME/plugins/wyngrep/$RAW_FILENAME
    else
        echo "Dependency [$i] found. Ok!"
    fi;
done;

echo 'Done!'

