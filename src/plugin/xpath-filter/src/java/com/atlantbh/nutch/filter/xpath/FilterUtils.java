package com.atlantbh.nutch.filter.xpath;

import java.util.regex.Pattern;

import org.w3c.dom.Node;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class FilterUtils {
    
    private static final Logger log = Logger.getLogger(FilterUtils.class);
	
	/**
	 * Returns the same value. If null returns the defaultValue.
	 * 
	 * @param value The value to return if not null.
	 * @param defaultValue The value to return if null.
	 * @return value or defaultValue. Depends of value is null.
	 */
	public static <T> T getNullSafe(T value, T defaultValue) {
		return value == null?defaultValue:value;
	}
	
	/**
	 * Extracts the text content from the supplied node.
	 * 
	 * @param node The node to extract text from.
	 * @return String that contains the textual content of the node.
	 */
	public static String extractTextContentFromRawNode_OLD(Object node) {
		
		// Extract data
		String value = null;
		if (node instanceof Node) {
			value = ((Node) node).getTextContent();
		} else {
			value = String.valueOf(node);
		}
		
        log.debug("### extractTextContentFromRawNode(node=" + node + ") returning: " + value);
		return value;
	}
	public static String extractTextContentFromRawNode(Object n) {
		// Extract data
		String value = null;
		if (n instanceof Node) {
            Node node =  ((Node) n);
            String nodeValue = null;
            // if(node.hasAttributes()) {
                // NamedNodeMap nodeAttributes = node.getAttributes();
                // nodeValue = node.getAttributes().getNamedItem("value").getNodeValue();
            // }
            if(StringUtils.equals(node.getNodeName(), "input")) {
                log.debug("Node(input-tag): " + node + 
                            ": getLocalName()=" + node.getLocalName() + 
                            ", getNodeName()=" + node.getNodeName() + 
                            ", getNodeType=" + node.getNodeType() +
                            ", getNodeValue=" + node.getAttributes().getNamedItem("value").getNodeValue() +
                            ", getTextContent=" + node.getTextContent() +
                            ", attribute.value=" + nodeValue);

                value = node.getAttributes().getNamedItem("value").getNodeValue();
            } else if(StringUtils.equals(node.getNodeName(), "div")) {
                value = node.getTextContent();
            } else {
                value = node.getTextContent();
            }
		} else {
			value = String.valueOf(n);
		}

        log.debug("### extractTextContentFromRawNode(node=" + n + ") returning: " + value);
		return value;
	}

	/**
	 * Check's if the url math the regex. Regex null safe.
	 * 
	 * @param regex The regex to match against.
	 * @param url The data to match.
	 * @return True if the url matches the regex, otherwise false.
	 */
	public static boolean isMatch(String regex, String data) {
		
        log.debug("### Inside isMatch(regex=" + regex + ", data=" + data + ")");
		// Compile regex pattern
		Pattern pattern = null;
		if(regex != null) {
			pattern = Pattern.compile(regex);
		} 
					
		if(pattern != null) {
			if(pattern.matcher(data).matches()) {
                log.debug("### isMatch(" + regex + ", " + data + ") returning: true");
				return true;		
			}
		} else {
            log.debug("### isMatch(" + regex + ", " + data + ") returning: true");
			return true;
		}

        log.debug("### isMatch(" + regex + ", " + data + ") returning: false");
		return false;
	}
	
	/**
	 * Checks if the string is entirely made of the provided characters.
	 * 
	 * @param string The string to check.
	 * @param charactesr The characters to check against.
	 * @return True if the string is entirely made of the characters supplied, otherwise false.
	 */
	public static boolean isMadeOf(String string, String characters) {
		
		// Initialize StringBuilder
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.setLength(1);
		
		// Iterate trough string and check if it contains one of the characters
		for(int i=0;i<string.length();i++) {
			
			stringBuilder.setCharAt(0, string.charAt(i));
			if(!characters.contains(stringBuilder)) {
				return false;
			}
		}
		
		return true;
	}
}
