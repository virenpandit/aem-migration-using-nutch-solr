package com.atlantbh.nutch.filter.xpath;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.PatternSyntaxException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.HtmlParseFilter;
import org.apache.nutch.parse.ParseResult;
import org.apache.nutch.parse.ParseStatus;
import org.apache.nutch.protocol.Content;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jaxen.JaxenException;
import org.jaxen.XPath;
import org.jaxen.dom.DOMXPath;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.atlantbh.nutch.filter.xpath.config.XPathFilterConfiguration;
import com.atlantbh.nutch.filter.xpath.config.XPathIndexerProperties;
import com.atlantbh.nutch.filter.xpath.config.XPathIndexerPropertiesField;

/**
 * A Xml-Html xpath filter implementation that fetches data
 * from the content, depending on the supplied xpath,
 * and prepares it for the {@link XPathIndexingFilter} to
 * index it into solr.
 * 
 * @author Emir Dizdarevic
 * @version 1.4
 * @since Apache Nutch 1.4
 */
public class XPathHtmlParserFilter implements HtmlParseFilter {
	
	// Constants
	private static final Logger log = Logger.getLogger(XPathHtmlParserFilter.class);
	private static final List<String> htmlMimeTypes = Arrays.asList(new String[] {"text/html", "application/xhtml+xml"});
	
	// OLD WAY TO DETERMIN IF IT'S AN XML FORMAT
	//private static final List<String> xmlMimeTypes = Arrays.asList(new String[] {"text/xml", "application/xml"});
	
	// Configuration
	private Configuration configuration;
	private XPathFilterConfiguration xpathFilterConfiguration;
	private String defaultEncoding;
	
	// Internal data
	private HtmlCleaner cleaner;
	private DomSerializer domSerializer;
	private DocumentBuilder documentBuilder;

	public XPathHtmlParserFilter() {
		init();
	}

	private void init() {
		
		// Initialize HTMLCleaner
		cleaner = new HtmlCleaner();
		CleanerProperties props = cleaner.getProperties();
		props.setAllowHtmlInsideAttributes(true);
		props.setAllowMultiWordAttributes(true);
		props.setRecognizeUnicodeChars(true);
		props.setOmitComments(true);
		props.setNamespacesAware(false);
		
		// Initialize DomSerializer
		domSerializer = new DomSerializer(props);
		
		// Initialize xml parser		
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// THIS CAN NEVER HAPPEN
		}
	}
	
	private void initConfig() {

		// Initialize configuration
		xpathFilterConfiguration  = XPathFilterConfiguration.getInstance(configuration);
		defaultEncoding = configuration.get("parser.character.encoding.default", "UTF-8");
	}

	@Override
	public Configuration getConf() {
		return configuration;
	}

	@Override
	public void setConf(Configuration configuration) {
		this.configuration = configuration;
		initConfig(); 
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public ParseResult filter(Content content, ParseResult parseResult, HTMLMetaTags metaTags, DocumentFragment doc) {
        log.info("### Inside filter()");
		Metadata metadata = parseResult.get(content.getUrl()).getData().getParseMeta();
		byte[] rawContent = content.getContent();
		
		try {
			Document cleanedXmlHtml = documentBuilder.newDocument();
			if(htmlMimeTypes.contains(content.getContentType())) {
                
                log.debug("### This document is of a supported content-type");
				
				// Create reader so the input can be read in UTF-8
				Reader rawContentReader = new InputStreamReader(new ByteArrayInputStream(rawContent), FilterUtils.getNullSafe(metadata.get(Metadata.                                                    ORIGINAL_CHAR_ENCODING), defaultEncoding));

				// Use the cleaner to "clean" the HTML and return it as a TagNode object
				TagNode tagNode = cleaner.clean(rawContentReader);

                String rawContentStr = new String(rawContent);
                // log.debug("### rawContent of Html=[" + rawContentStr + "]");

                // cleanedXmlHtml = documentBuilder.parse(new ByteArrayInputStream(rawContent));

				cleanedXmlHtml = domSerializer.createDOM(tagNode);

                // log.debug("### Inner-Contents of the document: " + cleaner.getInnerHtml(tagNode) + 
                            // ", cleanedXmlHtml=" + getStringFromDocument(cleanedXmlHtml));
			} else if(content.getContentType().contains(new StringBuilder("/xml")) || content.getContentType().contains(new StringBuilder("+xml"))) {

				// Parse as xml - don't clean
				cleanedXmlHtml = documentBuilder.parse(new InputSource(new ByteArrayInputStream(rawContent)));	
			} 

			// Once the HTML is cleaned, then you can run your XPATH expressions on the node, 
			// which will then return an array of TagNode objects 
            log.debug("Loading xpathfilter-conf.xml");
			List<XPathIndexerProperties> xPathIndexerPropertiesList = xpathFilterConfiguration.getXPathIndexerPropertiesList();
            log.debug("Iterating through XPath config rules, Object xPathIndexerPropertiesList=" + xPathIndexerPropertiesList);
			for(XPathIndexerProperties xPathIndexerProperties : xPathIndexerPropertiesList) {

				//****************************
				// CORE XPATH EVALUATION
				//****************************
                log.debug("Calling pageToProcess() for url: " + content.getUrl());
				if(pageToProcess(xPathIndexerProperties, cleanedXmlHtml, content.getUrl())) {
                    
                    log.debug("### pageToProcess() for url: " + content.getUrl() + "returned true");

					List<XPathIndexerPropertiesField> xPathIndexerPropertiesFieldList = xPathIndexerProperties.getXPathIndexerPropertiesFieldList();
					for(XPathIndexerPropertiesField xPathIndexerPropertiesField : xPathIndexerPropertiesFieldList) {
						
						// Evaluate xpath			
						XPath xPath = new DOMXPath(xPathIndexerPropertiesField.getXPath());
                        log.debug("### Evaluating if XPath applies: " + xPath + " to page: " + content.getUrl());
						List nodeList = xPath.selectNodes(cleanedXmlHtml);
                        if(null == nodeList) {
                            log.debug("### [Page: " + content.getUrl() + 
                                        "] Evaluation against XPath: [" + xPath + "] resulted in no matching nodes. nodeList is null");
                        } else {
                            log.debug("### [Page: " + content.getUrl() + 
                                        "] Evaluation against XPath: [" + xPath + "] resulted match(es): matching nodeList.size()=" + nodeList.size());
                        }

						// Trim?
                        log.debug("### xPathIndexerPropertiesField.getTrimXPathData()=" + xPathIndexerPropertiesField.getTrimXPathData());
						boolean trim = FilterUtils.getNullSafe(xPathIndexerPropertiesField.getTrimXPathData(), true);
                        
                        boolean multivalued = FilterUtils.getNullSafe(xPathIndexerPropertiesField.getMultivalued(), false);
                        log.debug("### xPathIndexerPropertiesField.multivalued=" + xPathIndexerPropertiesField.getMultivalued());

                        boolean isConcatenated = FilterUtils.getNullSafe(xPathIndexerPropertiesField.isConcat(), false);
						if(isConcatenated) {
							// Iterate trough all found nodes
							String value = new String();
							String concatDelimiter = FilterUtils.getNullSafe(xPathIndexerPropertiesField.getConcatDelimiter(), "");
							for (Object node : nodeList) {
								// Extract data	
								String tempValue = FilterUtils.extractTextContentFromRawNode(node);
								tempValue = filterValue(tempValue, trim);

								// Concatenate tempValue to value
								if(StringUtils.isNotBlank(tempValue)) {
									if(value.isEmpty()) {
										value = tempValue;
									} else {
										value = value + concatDelimiter + tempValue;
									}
								}
							}
							// Add the extracted data to meta
							if(StringUtils.isNotBlank(value)) {
                                log.debug("### Adding META-DATA(1): name: " + xPathIndexerPropertiesField.getName() + ", value: " + value);
								metadata.add(xPathIndexerPropertiesField.getName(), value);

                                if(!multivalued && !isConcatenated) {
                                    log.debug("Breaking out of the loop, this field for XPath [" + xPath + "] with value [" + value + "] is single-valued only");
                                    break;
                                }
							}
						} else {
							// Iterate trough all found nodes
                            List<String> valueList = new ArrayList<String>();
							for (Object node : nodeList) {
								// Add the extracted data to meta
								String value = FilterUtils.extractTextContentFromRawNode(node);					
								value = filterValue(value, trim);
								if(StringUtils.isNotBlank(value)) {
                                    if(multivalued) {
                                        valueList.add(value);
                                    } else {
                                        log.debug("### Adding META-DATA(2): name: " + xPathIndexerPropertiesField.getName() + ", value: " + value);
                                        metadata.add(xPathIndexerPropertiesField.getName(), value);

                                        if(!isConcatenated) {
                                            log.debug("Breaking out of the loop, this field for XPath [" + xPath + "] with value [" + value + "] is single-valued only");
                                            break;
                                        }
                                    }
								}
							}
                            if(multivalued && valueList.size()>0) {
                                log.debug("Converting valueList of [" + valueList.size() + "] items to a String array");
                                String[] values = valueList.toArray(new String[valueList.size()]);
                                metadata.setMulti(xPathIndexerPropertiesField.getName(), values);
                                log.debug("Added multi-valued property to metadata. Key=" + xPathIndexerPropertiesField.getName() + ", value=" + values);
                            }
						}
						
					}
				}
			}
			
		} catch (IOException e) {
			// This can never happen because it's an in memory stream
		} catch(PatternSyntaxException e) {
			System.err.println(e.getMessage());
			log.error("### Error parsing urlRegex: ", e);
			return new ParseStatus(ParseStatus.FAILED, "Error parsing urlRegex: " + e.getMessage()).getEmptyParseResult(content.getUrl(), configuration);
		} catch (ParserConfigurationException e) {
			System.err.println(e.getMessage());
			log.error("### HTML Cleaning error: ", e);
			return new ParseStatus(ParseStatus.FAILED, "HTML Cleaning error: " + e.getMessage()).getEmptyParseResult(content.getUrl(), configuration);
		} catch (SAXException e) {
			System.err.println(e.getMessage());
			log.error("### XML parsing error: ", e);
			return new ParseStatus(ParseStatus.FAILED, "XML parsing error: " + e.getMessage()).getEmptyParseResult(content.getUrl(), configuration);
		} catch (JaxenException e) {
			System.err.println(e.getMessage());
			log.error("### XPath error: ", e);
			return new ParseStatus(ParseStatus.FAILED, "XPath error: " + e.getMessage()).getEmptyParseResult(content.getUrl(), configuration);
		}
		
        log.info("### Exiting filter()");
		return parseResult;
	}

    //method to convert Document to String
    public String getStringFromDocument(Document doc) {
        try {
           DOMSource domSource = new DOMSource(doc);
           StringWriter writer = new StringWriter();
           StreamResult result = new StreamResult(writer);
           TransformerFactory tf = TransformerFactory.newInstance();
           Transformer transformer = tf.newTransformer();
           transformer.transform(domSource, result);
           return writer.toString();
        } catch(TransformerException ex)
        {
           ex.printStackTrace();
           return null;
        }
    } 
	
	@SuppressWarnings("rawtypes")
	private boolean pageToProcess(XPathIndexerProperties xPathIndexerProperties, Document cleanedXmlHtml, String url) throws JaxenException {

        log.debug("### Inside pageToProcess(url=" + url + ")");
		boolean processPage = true;

		// *************************************
		// URL REGEX CONTENT PAGE FILTERING
		// *************************************
        log.debug("### getPageUrlFilterRegex=" + xPathIndexerProperties.getPageUrlFilterRegex());
		processPage = processPage && FilterUtils.isMatch(xPathIndexerProperties.getPageUrlFilterRegex(), url);

		// Check return status
		if (!processPage) {
            log.debug("### pageToProcess() returning false.1!");
			return false;
		}

		// *************************************
		// XPATH CONTENT PAGE FILTERING
		// *************************************

        log.debug("### xPathIndexerProperties.getPageContentFilterXPath()=[" + xPathIndexerProperties.getPageContentFilterXPath() + "]");
		if (StringUtils.isNotBlank(xPathIndexerProperties.getPageContentFilterXPath())) {
            log.debug("### pageContentFilterXPath()=" + xPathIndexerProperties.getPageContentFilterXPath());
			XPath xPathPageContentFilter = new DOMXPath(xPathIndexerProperties.getPageContentFilterXPath());
            log.debug("### xPathPageContentFilter=" + xPathPageContentFilter);
			List pageContentFilterNodeList = xPathPageContentFilter.selectNodes(cleanedXmlHtml);
            if(null!=pageContentFilterNodeList) {
                log.debug("### pageContentFilterNodeList.size=" + pageContentFilterNodeList.size());
            } else {
                log.debug("### pageContentFilterNodeList is null");
            }
			boolean trim = FilterUtils.getNullSafe(xPathIndexerProperties.isTrimPageContentFilterXPathData(), true);
			
			if (FilterUtils.getNullSafe(xPathIndexerProperties.isConcatPageContentFilterXPathData(), false)) {

                log.debug("### Iterating through nodes...");
				// Iterate trough all found nodes
				String value = new String();
				String concatDelimiter = FilterUtils.getNullSafe(xPathIndexerProperties.getConcatPageContentFilterXPathDataDelimiter(), "");

                log.debug("### pageContentFilterNodeList=" + pageContentFilterNodeList);
				for (Object node : pageContentFilterNodeList) {
					// Extract data
					String tempValue = FilterUtils.extractTextContentFromRawNode(node);
					tempValue = filterValue(tempValue, trim);
                    log.debug("### Extracting content for node: " + node + ", tempValue=" + tempValue);

					// Concatenate tempValue to value
					if(StringUtils.isNotBlank(tempValue)) {
						if (value.isEmpty()) {
							value = tempValue;
						} else {
							value = value + concatDelimiter + tempValue;
						}
					}
				}

                
				processPage = processPage && FilterUtils.isMatch(xPathIndexerProperties.getPageContentFilterRegex(), value);
                log.debug("Check if value: [" + value + 
                            "] is a match for regex: [" + 
                            xPathIndexerProperties.getPageContentFilterRegex() + 
                            "] returned: " + processPage);
			} else {
				for (Object node : pageContentFilterNodeList) {

					// Add the extracted data to meta
					String value = FilterUtils.extractTextContentFromRawNode(node);
					value = filterValue(value, trim);
					if(StringUtils.isNotBlank(value)) {
						processPage = processPage && FilterUtils.isMatch(xPathIndexerProperties.getPageContentFilterRegex(), value);
					}
				}
			}
		}

        log.debug("### Exiting pageToProcess() with value" + processPage);
		return processPage;
	}
	
	private String filterValue(String value, boolean trim) {

		String returnValue = null;
        
        value = StringUtils.chomp(StringUtils.strip(value));
		// Filter out empty strings and strings made of space, carriage return and tab characters
		if(StringUtils.isNotBlank(value) && !FilterUtils.isMadeOf(value, " \n\t")) {

			// Trim data?
			returnValue = trimValue(value, trim);

            if(StringUtils.equals(value, "0")) {
                returnValue = null;
            }
		}

		return returnValue == null ? null : StringEscapeUtils.unescapeHtml(returnValue);
	}

	private String trimValue(String value, boolean trim) {
		
		String returnValue;
		if (trim) {
			returnValue = StringUtils.lowerCase((StringUtils.deleteWhitespace(value)));
		} else {
			returnValue = value;
		}

		return returnValue;
	}
}
