package com.atlantbh.nutch.filter.index.omit;

import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.parse.Parse;
import org.apache.commons.lang.StringUtils;

import com.atlantbh.nutch.filter.index.omit.config.FilteringType;
import com.atlantbh.nutch.filter.index.omit.config.OmitIndexingFilterConfiguration;
import com.atlantbh.nutch.filter.index.omit.config.OmitIndexingFilterConfigurationEntry;

public class OmitIndexingFilter implements IndexingFilter {

	// Constants
	private static Logger log = Logger.getLogger(OmitIndexingFilter.class);

	// Configuration
	private Configuration configuration;
	private OmitIndexingFilterConfiguration omitIndexingFilterConfiguration;

	public OmitIndexingFilter() {
	}

	private void initConfig() {

		// Initialize configuration
		omitIndexingFilterConfiguration = OmitIndexingFilterConfiguration.getInstance(configuration);
	}

	@Override
	public Configuration getConf() {
		return configuration;
	}

	@Override
	public void setConf(Configuration configuration) {
		this.configuration = configuration;
		initConfig();
	}

	@Override
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url, CrawlDatum datum, Inlinks inlinks) throws IndexingException {

        log.debug("Inside filter(url=" + url + ")");
		// Prepare data
        boolean blockUrlOverridingAllAllows = false;
        boolean allowUrl = false;
		String realUrl = new String(url.getBytes()).substring(0, url.getLength());
		Metadata metadata = parse.getData().getParseMeta();

        log.debug("Checking against Omit-Rules");
		if (omitIndexingFilterConfiguration.getFilteringType() == FilteringType.WHITELIST) {
			for (OmitIndexingFilterConfigurationEntry omitIndexingFilterConfigurationEntry : omitIndexingFilterConfiguration.getOmitIndexingFilterConfigurationEntryList()) {

				switch (omitIndexingFilterConfigurationEntry.getTarget()) {
                    
                    case META_FIELD_NOT_EQUALS:
                        String metaName = omitIndexingFilterConfigurationEntry.getName();
                        String metaValue = metadata.get(FilterUtils.getNullSafe(metaName, ""));
                        if (StringUtils.isNotBlank(omitIndexingFilterConfigurationEntry.getRegex()) && StringUtils.isNotBlank(metaValue)) {
                            Pattern blockPattern = Pattern.compile(omitIndexingFilterConfigurationEntry.getRegex());
                            if (blockPattern.matcher(metaValue).matches()) {
                                log.debug("[Omit Page] META_FIELD_NOT_EQUALS Check for meta: " + metaName + " with value: " + metaValue + " matched against block blockPattern: " + omitIndexingFilterConfigurationEntry.getRegex() + ". This page will be SKIPPED!");
                                blockUrlOverridingAllAllows = true;
                            }
                        }
                        break;

					case URL:		
						Pattern pattern = Pattern.compile(omitIndexingFilterConfigurationEntry.getRegex());
						if (pattern.matcher(realUrl).matches()) {
                            allowUrl = true;
							// return doc;
						}
						break;

					case META_FIELD_PRESENT:
						if(metadata.get(FilterUtils.getNullSafe(omitIndexingFilterConfigurationEntry.getName(), "")) != null) {
							allowUrl = true;
                            // return doc;
						}
						break;
				}
			}
			
			// return null;
            if(!blockUrlOverridingAllAllows && allowUrl) {
                log.debug("Exiting filter with VALID document for url: " + realUrl);
                return doc;
            } else {
                log.debug("Skipping this page: Exiting filter with NULL-document for url: " + realUrl);
                return null;
            }

		} else if (omitIndexingFilterConfiguration.getFilteringType() == FilteringType.BLACKLIST) {
			for (OmitIndexingFilterConfigurationEntry omitIndexingFilterConfigurationEntry : omitIndexingFilterConfiguration.getOmitIndexingFilterConfigurationEntryList()) {
					
				switch (omitIndexingFilterConfigurationEntry.getTarget()) {
					case URL:
						
						Pattern pattern = Pattern.compile(omitIndexingFilterConfigurationEntry.getRegex());
						if (pattern.matcher(realUrl).matches()) {
							return null;
						}

						break;
					case META_FIELD_PRESENT:
						
						if(metadata.get(FilterUtils.getNullSafe(omitIndexingFilterConfigurationEntry.getName(), "")) != null) {
							return null;
						}
						
						break;
				}
			}
			
			return doc;

		}

		return null;
	}

}
