#!/bin/bash

APP_NAME=indexer-aem

NUTCH_RUNTIME=/opt/nutch/runtime/local
for i in `ls ./conf/*`; do
    RAW_FILENAME=`basename $i`
    if [ -f "$NUTCH_RUNTIME/conf/$RAW_FILENAME" ]; then
        echo "OVERWRITING configuration-xml: [conf/$RAW_FILENAME]"
    else 
        echo "Copying configuration-xml: [conf/$RAW_FILENAME]"
    fi;
    /bin/cp -rf $i $NUTCH_RUNTIME/conf/$RAW_FILENAME
done;
echo 'Config Xmls Deployed!'

