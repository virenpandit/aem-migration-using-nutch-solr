#!/bin/bash

APP_NAME=indexer-aem
/bin/cp -rf /opt/nutch/build/$APP_NAME /opt/nutch/runtime/local/plugins/
echo "JAR file copied from [/opt/nutch/build/$APP_NAME] to [/opt/nutch/runtime/local/plugins/]"

/bin/cp -rf /opt/nutch/src/plugin/$APP_NAME/plugin.xml /opt/nutch/runtime/local/plugins/$APP_NAME/
echo "Plugin XML copied from [/opt/nutch/src/plugin/$APP_NAME/plugin.xml] to [/opt/nutch/runtime/local/plugins/$APP_NAME/]"

NUTCH_RUNTIME=/opt/nutch/runtime/local
./configs.sh
for i in `ls ./lib/*`; do
    RAW_FILENAME=`basename $i`
    if [ ! -f "$NUTCH_RUNTIME/lib/$RAW_FILENAME" ]; then
        echo "Copying dependency: [$RAW_FILENAME]"
        /bin/cp -rf $i $NUTCH_RUNTIME/lib/$RAW_FILENAME
    else
        echo "Dependency [$i] found. Ok!"
    fi;
done;

echo 'Done!'

