/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nutch.indexwriter.aem;

public interface AEMConstants {

    public static final String AEM_PREFIX = "aem.";
    public static final String AEM_MAPPING_XML = AEM_PREFIX + "filter.file";
    public static final String SERVER_URL = AEM_PREFIX + "server.url";
    public static final String SERVER_USERNAME = AEM_PREFIX + "auth.username";
    public static final String SERVER_PASSWORD = AEM_PREFIX + "auth.password";
    public static final String COMMIT_SIZE = AEM_PREFIX + "commit.size";
    public static final String MAPPING_FILE = AEM_PREFIX + "mapping.file";
    
    public static final String PAGETYPE_HOME = "home";
    public static final String PAGETYPE_OURHOTELS = "ourhotels";
    public static final String PAGETYPE_OURCULTURE = "ourculture";
    public static final String PAGETYPE_YOURREWARDS = "yourrewards";
    public static final String PAGETYPE_OURCITIES = "ourcities";
    public static final String PAGETYPE_YOURMEETINGS = "yourmeetings";
    public static final String PAGETYPE_SWEETDEALS = "sweetdeals";
    
    public static final String LITERAL = "LITERAL";
    public static final String META = "META";
    public static final String CONTAINS = "CONTAINS";
    public static final String DOES_NOT_CONTAIN = "DOES_NOT_CONTAIN";
    public static final String IS_MISSING = "IS_MISSING";
}
