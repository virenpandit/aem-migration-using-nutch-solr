//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.20 at 08:23:19 PM CEST 
//


package org.apache.nutch.indexwriter.aem.xjc.generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.apache.nutch.indexwriter.aem.xjc.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.apache.nutch.indexwriter.aem.xjc.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AemMappingConfig }
     * 
     */
    public AemMappingConfig createAemMappingConfig() {
        return new AemMappingConfig();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc }
     * 
     */
    public AemMappingConfig.Doc createAemMappingConfigDoc() {
        return new AemMappingConfig.Doc();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions }
     * 
     */
    public AemMappingConfig.Doc.Actions createAemMappingConfigDocActions() {
        return new AemMappingConfig.Doc.Actions();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions.Action }
     * 
     */
    public AemMappingConfig.Doc.Actions.Action createAemMappingConfigDocActionsAction() {
        return new AemMappingConfig.Doc.Actions.Action();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions.Action.Properties }
     * 
     */
    public AemMappingConfig.Doc.Actions.Action.Properties createAemMappingConfigDocActionsActionProperties() {
        return new AemMappingConfig.Doc.Actions.Action.Properties();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions.Action.Key }
     * 
     */
    public AemMappingConfig.Doc.Actions.Action.Key createAemMappingConfigDocActionsActionKey() {
        return new AemMappingConfig.Doc.Actions.Action.Key();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Conditions }
     * 
     */
    public AemMappingConfig.Doc.Conditions createAemMappingConfigDocConditions() {
        return new AemMappingConfig.Doc.Conditions();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions.Action.Properties.Property }
     * 
     */
    public AemMappingConfig.Doc.Actions.Action.Properties.Property createAemMappingConfigDocActionsActionPropertiesProperty() {
        return new AemMappingConfig.Doc.Actions.Action.Properties.Property();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Actions.Action.Key.Node }
     * 
     */
    public AemMappingConfig.Doc.Actions.Action.Key.Node createAemMappingConfigDocActionsActionKeyNode() {
        return new AemMappingConfig.Doc.Actions.Action.Key.Node();
    }

    /**
     * Create an instance of {@link AemMappingConfig.Doc.Conditions.Rule }
     * 
     */
    public AemMappingConfig.Doc.Conditions.Rule createAemMappingConfigDocConditionsRule() {
        return new AemMappingConfig.Doc.Conditions.Rule();
    }

}
