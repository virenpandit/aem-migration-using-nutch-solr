package org.apache.nutch.indexwriter.aem;

import javax.jcr.Repository; 
import javax.jcr.Session; 
import javax.jcr.SimpleCredentials; 
import javax.jcr.Node;
import org.apache.jackrabbit.commons.JcrUtils;

import java.io.IOException; 

import org.apache.hadoop.conf.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.mapred.JobConf;

import java.io.Reader;
import java.io.BufferedReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.nutch.indexwriter.aem.AEMConstants;
import org.apache.nutch.indexwriter.aem.xjc.generated.AemMappingConfig;
import org.apache.nutch.indexwriter.aem.xjc.generated.ObjectFactory;


public class AEMUtils implements AEMConstants {

    public static Logger LOG = LoggerFactory.getLogger(AEMUtils.class);

    public AemMappingConfig initConfig(Configuration configuration) {

        AemMappingConfig aemMappingConfig = null;
        try {
            LOG.debug("Inside initConfig(configuration.get(AEM_MAPPING_XML)=" + configuration.get(AEM_MAPPING_XML) + ")");

            LOG.debug("configuration.getConfResourceAsReader(configuration.get(AEM_MAPPING_XML))=" + configuration.getConfResourceAsReader(configuration.get(AEM_MAPPING_XML)));
            BufferedReader tmpReader = new BufferedReader(configuration.getConfResourceAsReader(configuration.get(AEM_MAPPING_XML)));
            StringBuffer sb = new StringBuffer();
            while(tmpReader.ready()) {
                sb.append(tmpReader.readLine());
            }
            LOG.debug(sb.toString());

            Reader configReader = configuration.getConfResourceAsReader(configuration.get(AEM_MAPPING_XML));

            LOG.debug("Loading JAXB for aemconfig-mapping");
            JAXBContext context = JAXBContext.newInstance(org.apache.nutch.indexwriter.aem.xjc.generated.ObjectFactory.class);
            // Or - JAXBContext context = JAXBContext.newInstance(new Class[] {AemMappingConfig.class});

            Unmarshaller unmarshaller = context.createUnmarshaller();

            LOG.debug("Unmarshalling aemconfig-mapping XML");
            aemMappingConfig  = (AemMappingConfig) unmarshaller.unmarshal(configReader);

            LOG.debug("Exiting initConfig with aemMappingConfig: " + aemMappingConfig);
        } catch(Exception ex) {
            LOG.error("Exception in initConfig()", ex);
        }
        return aemMappingConfig;
    }


    public Session login(String aemUrl, String aemUsername, String aemPassword) throws IOException {

        LOG.debug("Inside login(aemUrl=" + aemUrl + ", aemUsername=" + aemUsername + ")");

        Session session = null;

        try {
            Repository repository = JcrUtils.getRepository(aemUrl);

            LOG.debug("Before login");
            session = repository.login(new SimpleCredentials(aemUsername, aemPassword.toCharArray()), "crx.default");
            LOG.debug("Connected to AEM repository at: " + aemUrl);

            // Node root = session.getRootNode(); //Or getNode("/content");
            // Store content 
            // Node day = root.addNode("adobe");
            // day.setProperty("message", "Adobe Experience Manager is part of the Adobe Digital Product Suite");
            // session.save();
            // Retrieve content 
            // Node node = root.getNode("adobe"); 
            // LOG.debug(node.getPath()); 
            // LOG.debug(node.getProperty("message").getString());
            // Retrieve Attribute Array
            // // Retrieve content 
            // Node node = root.getNode("adobe/cq"); 
            // Property references = node.getProperty("testprop");  
            // Value[] values = references.getValues();
            // String myVal = values[0].getString();
            // session.logout();
            
        } catch(Exception ex) {
            LOG.error("Could not connect to repository", ex.toString());
            throw new IOException(ex);
            //ex.printStackTrace();
        }

        LOG.debug("Exiting login(session=" + session + ")");
        return session;
    }  
}
