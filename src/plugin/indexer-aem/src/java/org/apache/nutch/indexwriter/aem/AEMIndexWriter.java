package org.apache.nutch.indexwriter.aem;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapred.JobConf;
import org.apache.nutch.indexer.IndexWriter;
import org.apache.nutch.indexer.IndexerMapReduce;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.indexer.NutchField;
// import org.apache.nutch.metadata.Metadata;
import org.apache.commons.lang.StringUtils;

import javax.jcr.Repository; 
import javax.jcr.Session; 
import javax.jcr.SimpleCredentials; 
import javax.jcr.Node;
import org.apache.jackrabbit.commons.JcrUtils;

import java.util.Map.Entry;
import java.io.IOException;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.nutch.indexwriter.aem.xjc.generated.AemMappingConfig;
import org.apache.nutch.indexwriter.aem.xjc.generated.ObjectFactory;


public class AEMIndexWriter implements IndexWriter, AEMConstants {

    public static final Logger LOG = LoggerFactory.getLogger(AEMIndexWriter.class);

    private Configuration configuration;
    private String aemUrl = null;
    private String aemUsername = null;
    private String aemPassword = null;

    private AEMUtils aemUtils = new AEMUtils();
    private static AemMappingConfig aemMappingConfig;


    @Override
    public void open(JobConf job, String name) throws IOException {
        LOG.debug("Inside open, job=" + job + ", name=" + name);

        this.aemUrl = job.get(SERVER_URL, "http://localhost:4502/crx/server");
        this.aemUsername = job.get(SERVER_USERNAME, "admin");
        this.aemPassword = job.get(SERVER_PASSWORD, "admin");

        // session = aemUtils.login(aemUrl, aemUsername, aemPassword);
        // init(server, job);
    }

    @Override
    public void write(NutchDocument nutchDoc) throws IOException {
        LOG.debug("Inside write, doc=" + nutchDoc);

        // Metadata allMetadata = nutchDoc.getDocumentMeta();

        Session session = aemUtils.login(aemUrl, aemUsername, aemPassword);

        try {
            if(null!=aemMappingConfig && null!=aemMappingConfig.getDoc()) {
            LOG.debug("Evaluating rules from: " + AEM_MAPPING_XML + ", total rules=" + aemMappingConfig.getDoc().size());
                for( AemMappingConfig.Doc doc: aemMappingConfig.getDoc() ) {
                    // LOG.debug("Evaluating against aemconfig-mapping doc: " + doc.getId());
                    boolean matchesMappingRules = true;

                    if(null!=doc.getConditions() && null!=doc.getConditions().getRule()) {
                        // LOG.debug("Evaluating total of " + doc.getConditions().getRule().size() + " rules");
                        for( AemMappingConfig.Doc.Conditions.Rule rule: doc.getConditions().getRule()) {
                            if(!matchesDocument(nutchDoc, rule.getMatchKey(), rule.getRegex(), rule.getMatchType())) {
                                // LOG.debug("Skipping this aemconfig-mapping entry...Rules did not match");
                                matchesMappingRules = false;
                                break;
                            }
                        }
                        if(matchesMappingRules) { // Rules match. Let's save this into AEM as a document
                            if(null!=doc.getActions() && null!=doc.getActions().getAction()) {
                                LOG.debug("Executing a total of " + doc.getActions().getAction().size() + " actions");
                                for( AemMappingConfig.Doc.Actions.Action action: doc.getActions().getAction()) {
                                    // Lets create the node hierarchy first
                                    LOG.debug("Executing Actions: " + action.getId());
                                    String nodeName = "";
                                    javax.jcr.Node jcrNode = null;
                                    // LOG.debug("Creating JCR Node structure");
                                    for( AemMappingConfig.Doc.Actions.Action.Key.Node node: action.getKey().getNode()) {
                                        // nodeName = nodeName + "/" + getJCRNodeName(nutchDoc, node.getName(), node.getType());
                                        // jcrNode = createJCRNodeIfMissing(nodeName, session);
                                        nodeName = getJCRNodeName(nutchDoc, node.getName(), node.getType());
                                        LOG.debug("for-loop: Evaluating nodeName: " + nodeName);
                                        jcrNode = createJCRNodeIfMissing(nodeName, "cq:Page", jcrNode, session);
                                    }

                                    // LOG.debug("Setting JCR Node properties");
                                    // Now that we have the hierarchy all created and the content jcrNode, let's add attributes to it
                                    if(null!=jcrNode) {
                                        for( AemMappingConfig.Doc.Actions.Action.Properties.Property property: action.getProperties().getProperty() ) {
                                            
                                            addNodeProperty(jcrNode, property.getName(), nutchDoc.getFieldValue(property.getKey()), session);

                                            // if(nutchDoc.getFieldValue(property.getName()) instanceof String[]) {
                                                // LOG.debug("Adding Property (multivalued): name: " + property.getName() + ", value=" + nutchDoc.getFieldValue(property.getKey()));
                                                // jcrNode.setProperty(property.getName(), (String)nutchDoc.getFieldValue(property.getKey()));
                                            // } else {
                                                // LOG.debug("Adding Property: name: " + property.getName() + ", value=" + nutchDoc.getFieldValue(property.getKey()));
                                                // jcrNode.setProperty(property.getName(), (String)nutchDoc.getFieldValue(property.getKey()));
                                            // }
                                        }
                                    }
                                    LOG.debug("Saving node info to session");
                                    session.save();
                                }
                            } else {
                                LOG.debug("No Actions defined for this document?");
                            }
                        }
                    } else {
                        LOG.warn("No conditions/rules in doc? Skipping this doc");
                    }
                }
            } else {
                LOG.warn("aemMappingConfig.getDoc() returned null. No Rules defined?");
            }
        } catch(Exception ex) {
            LOG.error("Exception while saving document to AEM", ex);
        }
        finally {
            try {
                session.logout();
            } catch(Exception iex1) { }
        }
    }


    // String array comparison not supported yet!
    private boolean matchesDocument(NutchDocument nutchDoc, String matchKey, String regex, String matchType) {
        boolean isMatch = false;
        // LOG.debug("Inside matchesDocument: Checking if Nutch has field with matchKey: "  + matchKey + 
                    // " and matching pattern: " + regex + " and matchType: " + matchType);
        if(null!=nutchDoc && null!=nutchDoc.getFieldNames()) {
            // LOG.debug("nutchDoc.getFieldNames().size(): " + nutchDoc.getFieldNames().size());
            matchType = StringUtils.defaultIfBlank(matchType, CONTAINS);
            switch (matchType) {
                case DOES_NOT_CONTAIN:
                    isMatch = true;
                    for( String fieldName: nutchDoc.getFieldNames() ) {
                        Pattern pattern = Pattern.compile(regex);
                        // LOG.debug("Eval: NutchField(fieldName=" + fieldName + ", value: " + nutchDoc.getFieldValue(fieldName) + ") against Pattern(matchKey: " + matchKey + ", value: " + regex + ")");
                        if(StringUtils.equalsIgnoreCase(fieldName, matchKey) && pattern.matcher((String)nutchDoc.getFieldValue(fieldName)).matches()) {
                            // LOG.debug("matchesDocument: NutchField with matchKey: "  + matchKey +
                                        // ", pattern: " + regex + ", matchType: " + matchType + " matches crawled field: " + 
                                        // nutchDoc.getFieldValue(fieldName));
                            isMatch = false;
                            break;
                        }
                    }
                    break;
                case IS_MISSING:
                    isMatch = !nutchDoc.getFieldNames().contains(matchKey);
                    break;
                case CONTAINS: default:
                    for( String fieldName: nutchDoc.getFieldNames() ) {
                        Pattern pattern = Pattern.compile(regex);
                        // LOG.debug("Eval: NutchField(fieldName=" + fieldName + ", value: " + nutchDoc.getFieldValue(fieldName) + ") against Pattern(matchKey: " + matchKey + ", value: " + regex + ")");
                        if(StringUtils.equalsIgnoreCase(fieldName, matchKey) && pattern.matcher((String)nutchDoc.getFieldValue(fieldName)).matches()) {
                            LOG.debug("matchesDocument: NutchField with matchKey: "  + matchKey + 
                                        " and pattern: " + regex + ", matchType: " + matchType + " matches crawled field: " + 
                                        nutchDoc.getFieldValue(fieldName));
                            isMatch = true;
                            break;
                        }
                    }
                    break;
            }
        } else {
            LOG.warn("Nutch document has no fields!");
        }
        LOG.debug("matchesDocument: criteria: matchKey:"  + matchKey + ", regex:" + regex + ", matchType:" + matchType + " returning " + isMatch);
        return isMatch;
    }

    // Node name cannot be multi-valued
    private String getJCRNodeName(NutchDocument nutchDoc, String mappingNodeName, String mappingNodeType) {
        if( StringUtils.equalsIgnoreCase(mappingNodeType, LITERAL)) {
            LOG.debug("getJCRNodeName(): Returning (LITERAL): " + mappingNodeName);
            return mappingNodeName;
        } else {
            LOG.debug("getJCRNodeName(): Returning Field: " + nutchDoc.getFieldValue(mappingNodeName));
            return (String)nutchDoc.getFieldValue(mappingNodeName);
        }
    }

    private void addNodeProperty(Node node, String propertyName, Object value, Session session) throws Exception {
        LOG.debug("Inside addNodeProperty(node=" + node + ", type=" + node.getPrimaryNodeType() + ")");
        if(node.getPrimaryNodeType().isNodeType("cq:Page")) {
            LOG.debug("Node is of type: cq:Page: Adding jcr:content if missing");
            node = createJCRNodeIfMissing("jcr:content", "cq:PageContent", node, session);
        }
        if(value instanceof String[]) {
            node.setProperty(propertyName, (String[])value);
        } else {
            LOG.debug("Adding Property: propertyName: " + propertyName + ", value=" + value + " to node: " + node.getName());
            node.setProperty(propertyName, (String)value);
        }
    }

    private Node createJCRNodeIfMissing(String name, String nodeType, Node parentNode, Session session) throws Exception {
        Node node = null;
        if(null==parentNode) {
            parentNode = session.getRootNode();
        }
        LOG.debug("Inside createJCRNodeIfMissing(node=" + name + ")");
        
        LOG.debug("Checking first if node exists: " + name + " under Parent: [" + parentNode.getPath() + "]");
        if(parentNode.hasNode(name)) {
            LOG.debug("createJCRNodeIfMissing(): Node: [" +  name + "] already exists under Parent: [" + parentNode.getPath() + "] No need to recreate");
            node = parentNode.getNode(name);
        } else {
            LOG.debug("This node does not exist [" + name + "] under Parent: [" + parentNode.getPath() + "]");
            if(null!=parentNode) {
                LOG.debug("createJCRNodeIfMissing(): Creating node: [" + name + "] under parent: [" + parentNode.getName() + "]");
                node = parentNode.addNode(name, nodeType);
                if(StringUtils.equalsIgnoreCase(nodeType, "cq:Page")) {
                    LOG.debug("Adding PageContent (jcr:content) sub-node to node: " + node.getName());
                    createJCRNodeIfMissing("jcr:content", "cq:PageContent", node, session);
                }
            } else {
                LOG.debug("createJCRNodeIfMissing(): Creating node: [" + name + "] at /");
                node = session.getRootNode().addNode(name, nodeType);
            }
        }
        if(null!=node)
            LOG.debug("Exiting createJCRNodeIfMissing(node=" + node.getName() + ")");
        else
            LOG.debug("Exiting createJCRNodeIfMissing(node=" + node + ")");
        return node;
    }
    // private Node createJCRNodeIfMissing(String name, Session session) throws Exception {
        // Node node = null;
        // if(session.nodeExists(name)) {
            // LOG.debug("createJCRNodeIfMissing(): Node: " + name + " already exists. No need to recreate");
            // node = session.getNode(name);
        // } else {
            // LOG.debug("createJCRNodeIfMissing(): Creating node: " + name);
            // node = session.getRootNode().addNode(name, "cq:Page");
        // }
        // return node;
    // }


    @Override
    public void update(NutchDocument doc) throws IOException {
        LOG.debug("Inside update, doc=" + doc);
        write(doc);
    }
    
    @Override
    public void delete(String key) throws IOException {
        LOG.debug("Inside delete, key=" + key);
    }

    @Override
    public void close() throws IOException { 
        LOG.debug("Inside close()");
    }
    
    @Override
    public void commit() throws IOException { 
        LOG.debug("Inside commit");
    }

    @Override
    public Configuration getConf() {
        LOG.debug("Inside getConf()");
        return this.configuration;
    }

    @Override
    public void setConf(Configuration conf) {
        LOG.debug("Inside setConf, conf=" + conf);
        this.configuration = conf;
        String serverURL = conf.get(SERVER_URL);
        if (serverURL == null) {
            String message = "Missing AEM URL. Or could be set via -D " + SERVER_URL;
            message += "\n" + describe();
            LOG.error(message);
            throw new RuntimeException(message);
        }

        if(null == this.aemMappingConfig) {
            // Initialize configuration from mapping XML
            this.aemMappingConfig  = aemUtils.initConfig(conf);
        } else {
            LOG.debug("AemMappingConfig already initialized");
        }
    }


    @Override
    public String describe() {
        LOG.debug("Inside describe()");
        StringBuffer sb = new StringBuffer("AEMIndexWriter\n");
        sb.append("\t").append(SERVER_URL)
                        .append(" : URL of the AEM instance (mandatory)\n");
        sb.append("\t")
                        .append(MAPPING_FILE)
                        .append(
                        " : name of the mapping file for fields (default aemindex-mapping.xml)\n");
        sb.append("\t").append(SERVER_USERNAME)
                        .append(" : username for authentication\n");
        sb.append("\t").append(SERVER_PASSWORD)
                        .append(" : password for authentication\n");
        return sb.toString();
    }
}
