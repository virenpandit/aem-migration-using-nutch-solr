#!/bin/bash

# Note: Generate XSD from Xml online at:
#   http://www.xmlforasp.net/CodeBank/System_Xml_Schema/BuildSchema/BuildXMLSchema.aspx
#   After generation, change Xml prolog from UTF-16 to UTF-8 if necessary

xjc -d src/java -p org.apache.nutch.indexwriter.aem.xjc.generated conf/aemconfig-mapping.xsd

